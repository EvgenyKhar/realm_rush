﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessMovement : MonoBehaviour
{
    [Range(0.5f, 5)] [SerializeField] private float timeSteps = 1f;
    [SerializeField]List<Waypoint> path = new List<Waypoint>();


    void Start()
    {
        StartCoroutine(FollowPath(path));
    }

    private IEnumerator FollowPath(List<Waypoint> path)
    {
        while (true)
        {
            foreach (var item in path)
            {
                transform.position = item.transform.position;
                yield return new WaitForSeconds(timeSteps);
            }
        }
    }
}
