﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealthSystem : MonoBehaviour
{
    [SerializeField] int hp = 20;
    [SerializeField] GameObject damageFx;
    [SerializeField] GameObject deathFx;
    [SerializeField] Text hpText;
    [SerializeField] AudioClip looseSFX;
    int damagePerHit=1;
    int deathExplCount = 3;
    float explosionDistance = 15f;


    void Start()
    {
        hpText.text = "HP: " + hp.ToString();
    }

    void OnTriggerEnter()
    {
       DamageBase();
    }

    private void DamageBase()
    {
        hp -= damagePerHit;
        hpText.text = "HP: "+hp.ToString();
        GameObject fx = Instantiate(damageFx, transform.position, Quaternion.identity, transform);
        if (hp <= 0)
        {
            StartDeathSequence(deathFx);
        }
    }

    private void StartDeathSequence(GameObject Fx)
    {
        StartCoroutine(ShowLoseImage());
        PlayLoseFX(Fx);
        Invoke("LoadMainMenu", looseSFX.length + 1f);
    }

    private void PlayLoseFX(GameObject Fx)
    {
        for (int i = 0; i < deathExplCount; i++)
        {
            GameObject fx = Instantiate(Fx, transform.position + Vector3.left * i * explosionDistance, Quaternion.identity, transform);
            AudioSource.PlayClipAtPoint(looseSFX, Camera.main.transform.position);
        }
    }

    private IEnumerator ShowLoseImage()
    {
        Image loseImage = GameObject.Find("Lose_Image").GetComponent<Image>();
        Color tempColor = loseImage.color;
        float waitingTime = looseSFX.length / 10;
        for (int i = 0; i < 10; i++)
        {
            tempColor.a = i / looseSFX.length * 10;
            loseImage.color = tempColor;
            yield return new WaitForSeconds(waitingTime);
        }
    }

    private void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
