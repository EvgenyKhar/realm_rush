﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    [Tooltip("in s for 2 first waves")][Range (0.5f, 10f)][SerializeField] float[] spawnRate = new float[2];
    [SerializeField] Transform spawningPos;
    [SerializeField] Text scoreText;
    [SerializeField] int winScore = 50;
    [SerializeField] AudioClip winSFX;

    float secondsBetweenSpawns = 3f;
    public int score=10;
    int enemyCost = 1;

    void Start()
    {
        ChangeScore(0);
        StartCoroutine(SpawningEnemy());
    }

    public void ChangeScore(int value)
    {
        score += value;
        scoreText.text = score.ToString();
    }

    IEnumerator SpawningEnemy()
    {
        
        while(score<winScore)
        {
            GameObject enemy = Instantiate(enemyPrefab, spawningPos.position, Quaternion.identity, gameObject.transform);
            ChangeScore(enemyCost);
            ChangeDifficulty(enemy.GetComponent<EnemyMover>());
            yield return new WaitForSeconds(secondsBetweenSpawns);
        }

        StartWinSequence();

    }


    private void StartWinSequence()
    {
        StartCoroutine(ShowWinImage());
        AudioSource.PlayClipAtPoint(winSFX, Camera.main.transform.position);
        Invoke("LoadMainMenu", winSFX.length + 1f);
    }
    private IEnumerator ShowWinImage()
    {
        Image winImage = GameObject.Find("Win_Image").GetComponent<Image>();
        Color tempColor = winImage.color;
        float waitingTime = winSFX.length / 10;
        for (int i = 0; i < 10; i++)
        {
            tempColor.a = i / winSFX.length * 10;
            winImage.color = tempColor;
            yield return new WaitForSeconds(waitingTime);
        }
    }

    private void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void ChangeDifficulty(EnemyMover enemy)
    {
        if (score > 12 && score < 20)
        {
            enemy.SetSpeed(1);
            secondsBetweenSpawns = spawnRate[1];
        }
        else if (score >= 20 && score < 40)
        {
            enemy.SetSpeed(2);
        }
        else if (score >= 40)
        {
            enemy.SetSpeed(3);
        }
        else
        {
            enemy.SetSpeed(1);
            secondsBetweenSpawns = spawnRate[0];
        }
    }
}
