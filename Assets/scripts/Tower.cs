﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] Transform objectToPan;
    [SerializeField] float atackRange = 40;
    [SerializeField] GameObject explosionFX;
    [SerializeField] int hp = 3;

    ParticleSystem.EmissionModule bullets;
    EnemyHealthSystem closestEnemy;
    public Waypoint baseBlock;

    void Start()
    {
        bullets = objectToPan.Find("Bullets").GetComponent<ParticleSystem>().emission;
    }
    
    void Update()
    {
        if (enemyInRangeExists())
        {
            objectToPan.LookAt(closestEnemy.transform);
            bullets.enabled = true; 
        }
        else
        {
            bullets.enabled = false;
        }
        
    }

    private bool enemyInRangeExists()
    {
        EnemyHealthSystem[] enemies = FindObjectsOfType<EnemyHealthSystem>();
        if (enemies.Length < 1)
        {
            return false;
        }
       
        return EnemyInRange(enemies);
        
    }

    private bool EnemyInRange(EnemyHealthSystem[] enemies)
    {
        closestEnemy = null;
        //we won't ba doing anything with enemies, which too way of tower
        //so, we are looking for alive enemies closer than atackRange
        float closestDistance = atackRange;
        float distanceToEnemy;
        bool enemyIsAlive; 
            foreach (EnemyHealthSystem enemy in enemies)
        {
            enemyIsAlive = enemy.GetComponentInParent<EnemyHealthSystem>().curState == EnemyHealthSystem.EnemyState.Alive;
            distanceToEnemy = Vector3.Distance(enemy.transform.position, objectToPan.position);

            if (distanceToEnemy < closestDistance && enemyIsAlive)
            {
                closestEnemy = enemy;
                closestDistance = Vector3.Distance(enemy.transform.position, objectToPan.position);
            }
        }

        return closestEnemy!=null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<EnemyHealthSystem>())
        {
            hp -= 1;
            if (hp <= 0)
            {
                StartDeathSequence(explosionFX);
            }
            
        }
    }

    private void StartDeathSequence(GameObject Fx)
    {
        FindObjectOfType<TowerFactory>().DelTower(this);
        baseBlock.isBusy = false;
        GameObject fx = Instantiate(Fx, transform.position, Quaternion.identity, gameObject.transform);
        Destroy(gameObject, Fx.GetComponent<AudioSource>().clip.length);
    }
}
