﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{

    Queue<Tower> towerQueue= new Queue<Tower>();
    [SerializeField] int towerLimit = 3;
    [SerializeField] Tower towerPrefab;
    [SerializeField] Transform parent;
    [SerializeField] int towerCost =5;
    [SerializeField] AudioClip needMoreGold;

    public void PlaceTower(Waypoint waypoint)
    {
        int currentScore = FindObjectOfType<EnemySpawner>().score;
        if (currentScore >= towerCost)
        {
            FindObjectOfType<EnemySpawner>().ChangeScore(-towerCost);
            if (towerQueue.Count < towerLimit)
            {
                Tower newTower = CreateTower(onWaypoint: waypoint);
                towerQueue.Enqueue(newTower);
            }
            else
            {
                MoveOldestTower(toWaypoint: waypoint);
            }
        }
        else
        {
            AudioSource.PlayClipAtPoint(needMoreGold, Camera.main.transform.position);
        }
    }

    private Tower CreateTower(Waypoint onWaypoint)
    {
        Tower tower = Instantiate(towerPrefab, onWaypoint.transform.position, Quaternion.identity, parent);
        tower.baseBlock = onWaypoint; 
        tower.baseBlock.isBusy = true;
        return tower;
    }

    private void MoveOldestTower(Waypoint toWaypoint)
    {
        Tower tower = towerQueue.Dequeue();
        tower.baseBlock.isBusy = false;
        tower.transform.position = toWaypoint.transform.position;
        tower.baseBlock = toWaypoint;
        tower.baseBlock.isBusy = true;
        towerQueue.Enqueue(tower);
        tower.GetComponent<AudioSource>().Play();
    }

    public void DelTower(Tower tower)
    {
        Tower tempTower;
        
        for (int i = 0; i < towerQueue.Count; i++)
        {
            tempTower = towerQueue.Dequeue();
            if(tempTower!=tower)
            {
                towerQueue.Enqueue(tempTower);
            }
        }
    }
}
