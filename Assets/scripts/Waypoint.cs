﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


public class Waypoint : MonoBehaviour
{

    const int gridSize = 10;
    Vector2Int gridPos;
    
    public bool isExplored = false;
    public Waypoint exploredFrom;
    public bool isBusy = false;
    public bool isOnPath = false;


    void Start()
    {
        Physics.queriesHitTriggers = true; //we can mark colliders as triggers, but this so will be better
    }

    public int GetGridSize()
    {
        return gridSize;
    }

    public Vector2Int GetGridPos()
    {

        return new Vector2Int(
            (int)Mathf.Round(transform.position.x / gridSize),
            (int)Mathf.Round(transform.position.z / gridSize)
            );
    }

    public void Explore(Waypoint from)
    {
        isExplored = true;
        exploredFrom = from;
    }
  
    void OnMouseOver()
    {
       //highlight the block
    }

    void OnMouseDown()
    {
        if (CrossPlatformInputManager.GetButton("Mouse X"))
        {
            if(!isBusy)
            { 
                PlaceTower();
                if(isOnPath)
                {
                   FindObjectOfType<PathFinder>().RecalculatePath();
                }
            }
            else
            {
                print(gameObject.name + " isn't placeable");
            }
            
        }

        
        
    }

    private void PlaceTower()
    {
            FindObjectOfType<TowerFactory>().PlaceTower(this);
    }
}
