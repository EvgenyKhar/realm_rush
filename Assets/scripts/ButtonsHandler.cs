﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsHandler : MonoBehaviour
{
    [SerializeField] GameObject infoBanner;
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowInfo(bool show)
    {
        infoBanner.SetActive(show);
    }
}
