﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    [Range(0.5f, 5)][SerializeField]private float[] timeSteps = new float[3];
    private float timeStep;
    List<Waypoint> ownPath= new List<Waypoint>();

    void Awake()
    {
        List<Waypoint> path = GameObject.Find("World").GetComponent<PathFinder>().GetPath();
        foreach (Waypoint waypoint in path)
        {
            ownPath.Add(waypoint);
        }
    }
    void Start()
    {
        StartCoroutine(FollowPath(ownPath));
    }

    private IEnumerator FollowPath(List<Waypoint> ownPath)
    {
        for(int i=0;i< ownPath.Count;i++)
        {
            if(gameObject.GetComponent<EnemyHealthSystem>().curState == EnemyHealthSystem.EnemyState.Alive)
            {
                transform.position =  ownPath[i].transform.position;
            }
            yield return new WaitForSeconds(timeStep);
        }
    }

    public void SetSpeed(int speed)
    {
         timeStep = timeSteps[speed-1];
    }
    


}
