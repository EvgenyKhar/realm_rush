﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthSystem : MonoBehaviour
{
    public enum EnemyState
    {
        Alive,
        Dying
    }

    BoxCollider boxCollider;
    [SerializeField] int hp = 10;
    [SerializeField] GameObject damageFx;
    [SerializeField] GameObject deathFx;
    [SerializeField] GameObject explosionFX;
    

    int damagePerHit;
    public EnemyState curState=EnemyState.Alive;
    private float fallingDistance= 3;

    void Start()
    {
        AddNonTriggerBoxCollider();
    }

    private void AddNonTriggerBoxCollider()
    {
        boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
        boxCollider.size = new Vector3(6f, 25f, 6f);
    }

    void OnParticleCollision(GameObject other)
    {
        damagePerHit = 1;// todo make various, depends on tower type
        DamageEnemy();
    }

    private void OnTriggerEnter(Collider other)
    {
            StartDeathSequence(explosionFX);
    }

    private void DamageEnemy()
    {
        hp -= damagePerHit; 
     GameObject fx = Instantiate(damageFx, transform.Find("Body").position, Quaternion.identity, gameObject.transform);
        if(hp<=0 && curState==EnemyState.Alive)
        {
            StartDeathSequence(deathFx);
        }
    }

    private void StartDeathSequence(GameObject Fx)
    {
        curState = EnemyState.Dying;
        gameObject.transform.position += Vector3.down* fallingDistance;
        gameObject.transform.eulerAngles += new Vector3(0f, 45f,0f);
        GameObject fx = Instantiate(Fx, transform.Find("Body").position, Quaternion.identity, gameObject.transform);
        Destroy(gameObject, Fx.GetComponent<AudioSource>().clip.length);
    }

    
}
