﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector = new Vector3(5f, 5f, 5f);

    //todo remove that later
    [Range(0,1)] [SerializeField] float movementAmplitude = 1f;//0 for not moving 1 is for full moving
    Vector3 startingPos;
    [SerializeField] private float frequence = 1f;
    float cycle;
    public Vector3 offset { get; private set; }
    const float tau = Mathf.PI*2;

    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
         
        
    }

    // Update is called once per frame
    void Update()
    {
        cycle = Time.time * frequence;

        offset = movementAmplitude * movementVector * Mathf.Sin(cycle*tau);
        transform.position = startingPos + offset;
    }
}
