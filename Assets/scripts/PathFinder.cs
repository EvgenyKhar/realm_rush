﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{

    Dictionary<Vector2Int, Waypoint> grid = new Dictionary<Vector2Int, Waypoint>();
    Queue<Waypoint> queue= new Queue<Waypoint>();
    bool isRunning = true;
    Waypoint searchCenter;
    List<Waypoint> path = new List<Waypoint>();
    [SerializeField] public Waypoint startWaypoint, endWaypoint;
    Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    public List<Waypoint> GetPath()
    {
        if (path.Count == 0)
        {
            CalculatePath();
        }
        return path;
    }

    private void CalculatePath()
    {
        LoadBlocksToWorld();
        BreadthFirstSearch();
        CreatePath();
    }

    private void LoadBlocksToWorld()
    {
        Waypoint[] waypoints = FindObjectsOfType<Waypoint>();

        foreach (Waypoint waypoint in waypoints)
        {
            if (!waypoint.isBusy)
            {
                AddBlockToWorld(waypoint);
            }   
        }
    }

    private void AddBlockToWorld(Waypoint waypoint)
    {
        if (grid.ContainsKey(waypoint.GetGridPos()))
        {
            Debug.LogWarning("Skipping Overlapping block " + waypoint);
        }
        else 
        {
            grid.Add(waypoint.GetGridPos(), waypoint);
        }
    }

    private void BreadthFirstSearch()
    {
        queue.Enqueue(startWaypoint);
        startWaypoint.isExplored = true;

        while (queue.Count > 0 && isRunning)
        {
            searchCenter = queue.Dequeue();
            HaltIfEndFound();
            ExploreNeighboors();
        }
    }

    private void HaltIfEndFound()
    {
        if (searchCenter == endWaypoint)
        {
            isRunning = false;
        }
    }

    private void ExploreNeighboors()
    {
        if(!isRunning) { return; }

        foreach (Vector2Int direction in directions)
        {
            
            Vector2Int neighboorCoordinates = searchCenter.GetGridPos() + direction;
            
            if( grid.ContainsKey(neighboorCoordinates))
            {
                QueuingNeighboor(neighboorCoordinates);
            }

        }
    }

    private void QueuingNeighboor(Vector2Int neighboorCoordinates)
    {
        Waypoint neighboor = grid[neighboorCoordinates];
        if (!neighboor.isExplored && !neighboor.isBusy)
        {
            neighboor.Explore(from: searchCenter);
            queue.Enqueue(neighboor);

        }
    }

    private void CreatePath()
    {
        Waypoint previous = endWaypoint;
        while (previous != startWaypoint)
        {
            previous.isOnPath = true;
            path.Add(previous);
            previous = previous.exploredFrom;
        }
        path.Add(startWaypoint);
        startWaypoint.isOnPath = true;
        path.Reverse();
    }

    public void RecalculatePath()
    {
        foreach (KeyValuePair<Vector2Int, Waypoint> item in grid)
        {
            item.Value.isExplored = false;
        }
        isRunning = true;
        path.Clear();
        grid.Clear();
        queue.Clear();
        CalculatePath();
    }
}
