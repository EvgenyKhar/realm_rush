﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent( typeof(Waypoint))]
public class CubeEditor : MonoBehaviour
{
    TextMesh textMesh;
    string labelText;
    Waypoint wayPoint;

    void Awake()
    {
        wayPoint = GetComponent<Waypoint>();
    }

    void Start()
    {
        textMesh = gameObject.GetComponentInChildren<TextMesh>();
    }

    void Update()
    {
        SnapToGrid();
        UpdateLabel();
    }

    private void SnapToGrid()
    {
        int gridSize = wayPoint.GetGridSize();
        transform.position = new Vector3(
            wayPoint.GetGridPos().x * gridSize,
            0f,
            wayPoint.GetGridPos().y* gridSize); 
        
    }

    private void UpdateLabel()
    {
       
        labelText = 
            wayPoint.GetGridPos().x+
            "," + 
            wayPoint.GetGridPos().y;
        textMesh.text = labelText;
        
        gameObject.name = labelText + " cube";
    }
}
